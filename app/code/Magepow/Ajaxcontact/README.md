**Magento 2 Ajaxcontact extension** (Magepow Ajaxcontact Extension) for Magento allows the customer to submit the form with ajax without redirect to the contact page. Additionally,  you can add to anywhere with widget use "Ajax Contact Form Widget". The Ajaxcontact block is Responsive Web Design (RWD) ready. It will recalculate itself based on the parent.
## 1. How to install Magento 2 Ajaxcontact
### ✓ Install Magepow Ajaxcontact via composer (recommend)
Run the following command in Magento 2 root folder:

`composer require magepow/ajaxcontact`

`php bin/magento setup:upgrade`

`php bin/magento setup:static-content:deploy -f`
## 2. Magepow Ajaxcontact user guide
**Magepow Ajaxcontact** allows add a contact form to any page quickly and conveniently.
### General Ajaxcontact
#### Add Ajax Contact Form Widget
Go to `Admin Panel > Content > Pages => Choose Page you want add contact from.`
![widget-img](https://github.com/magepow/magento2-ajax-contact/blob/master/media/ajaxcontact_widget.png)

Select `Ajax Contact From Widget` to add widget.
#### Save Page
![widget-sav-img](https://github.com/magepow/magento2-ajax-contact/blob/master/media/ajaxcontact_add.png)
### This Is Result In Frontend
 ![ajaxcontact_submit-img](https://github.com/magepow/magento2-ajax-contact/blob/master/media/ajaxcontact_submit.png)
 
 #### Result Ajax after submit form
 ![ajaxcontact_submit-img](https://github.com/magepow/magento2-ajax-contact/blob/master/media/ajaxcontact_result.png)

**Other free extensions**

* [Magento 2 Recent Sales Notification](https://magepow.com/magento-2-recent-sales-notification.html)

* [Magento Categories Extension](https://magepow.com/magento-categories-extension.html)

* [Magento Sticky Cart](https://magepow.com/magento-sticky-cart.html)

**Featured Magento services**

* [PSD to Magento 2 Theme Conversion](https://magepow.com/psd-to-magento-theme-conversion.html)

* [Magento Speed Optimization Service](https://magepow.com/magento-speed-optimization-service.html)

* [Magento Security Patch Installation](https://magepow.com/magento-security-patch-installation.html)

* [Magento Website Maintenance Service](https://magepow.com/website-maintenance-service.html)

* [Magento Professional Installation Service](https://magepow.com/professional-installation-service.html)

* [Magento Upgrade Service](https://magepow.com/magento-upgrade-service.html)

* [Customization Service](https://magepow.com/customization-service.html)

* [Hire Magento Developer](https://magepow.com/hire-magento-developer.html)

[![Latest Stable Version](https://poser.pugx.org/magepow/ajaxcontact/v/stable)](https://packagist.org/packages/magepow/ajaxcontact)
[![Total Downloads](https://poser.pugx.org/magepow/ajaxcontact/downloads)](https://packagist.org/packages/magepow/ajaxcontact)



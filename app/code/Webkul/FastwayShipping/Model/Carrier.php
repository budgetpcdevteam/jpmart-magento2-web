<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_FastwayShipping
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\FastwayShipping\Model;

use Magento\Catalog\Model\Product\Type;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Magento\Framework\Xml\Security;
use Magento\Framework\Session\SessionManager;
use Magento\Shipping\Model\Shipping\LabelGenerator;
use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;
use Magento\Framework\Module\Dir;

/**
 * Marketplace Fastway shipping.
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Carrier extends AbstractCarrierOnline implements \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * Code of the carrier.
     *
     * @var string
     */
    const CODE = 'webkulfastway';
    /**
     * Code of the carrier.
     *
     * @var string
     */
    protected $_code = self::CODE;
    /**
     * [$_coreSession description].
     *
     * @var [type]
     */
    protected $_coreSession;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     * Rate result data.
     *
     * @var Result|null
     */
    protected $_result = null;
    /**
     * @var array
     */
    protected $_totalPriceArr = [];

    /**
     * @var boolean
     */
    protected $_check = false;

    /**
     * @var boolean
     */
    protected $_flag = false;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_httpClientFactory;

    protected $_storeManager;

    protected $_currency;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface             $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory     $rateErrorFactory
     * @param \Psr\Log\LoggerInterface                                       $logger
     * @param Security                                                       $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory               $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory                     $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory    $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory                 $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory           $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory          $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory                         $regionFactory
     * @param \Magento\Directory\Model\CountryFactory                        $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory                       $currencyFactory
     * @param \Magento\Directory\Helper\Data                                 $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface           $stockRegistry
     * @param \Magento\Store\Model\StoreManagerInterface                     $storeManager
     * @param \Magento\Framework\Module\Dir\Reader                           $configReader
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param SessionManager                                                 $coreSession
     * @param \Magento\Customer\Model\Session                                $customerSession
     * @param LabelGenerator                                                 $labelGenerator
     * @param array                                                          $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Module\Dir\Reader $configReader,
        SessionManager $coreSession,
        \Magento\Customer\Model\Session $customerSession,
        LabelGenerator $labelGenerator,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Directory\Model\Currency $currency,
        array $data = []
    ) {
        $this->_httpClientFactory = $httpClientFactory;
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $data
        );
        $this->_coreSession = $coreSession;
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_currency = $currency;
    }

    /**
     * Collect and get rates.
     *
     * @param RateRequest $request
     *
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Error|bool|Result
     */
    public function collectRates(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        $requestFastway = clone $request;
        if (!$this->canCollectRates()) {
            return false;
        }
        $this->setRequest($requestFastway);
        $response = $this->getRequestParam();
        $result = $this->_rateFactory->create();

        if (empty($response)) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier('webkulfastway');
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        } else {
            foreach ($response as $method => $values) {
                $rate = $this->_rateMethodFactory->create();
                $currencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
                $price = $values['amount'];
                $rate->setCarrier($this->_code);
                $rate->setCarrierTitle($this->getConfigData('title'));
                $rate->setMethod($method);
                $rate->setMethodTitle($values['label']);
                $rate->setCost($price);
                $rate->setPrice($price);
                $result->append($rate);
            }
        }
         return $result;
    }

    /**
     * Check if carrier has shipping tracking option available
     * All \Magento\Usa carriers have shipping tracking option available
     *
     * @return boolean
     */
    public function isTrackingAvailable()
    {
        return false;
    }

    /**
     * Check if carrier has shipping label option available
     *
     * @return boolean
     */
    public function isShippingLabelsAvailable()
    {
        return false;
    }

    /**
     * Prepare and set request to this instance.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setRequest(\Magento\Framework\DataObject $request)
    {
        $this->_request = $request;

        $r = new \Magento\Framework\DataObject();

        $r->setDestCountryId($request->getDestCountryId());

        if ($request->getDestPostcode()) {
            $r->setDestPostal($request->getDestPostcode());
        }

        $r->setDestRegionCode($request->getDestRegionCode());
        if ($request->getDestCity()) {
            $r->setDestCity($request->getDestCity());
        }

        $r->setStoreId($request->getStoreId());

        $this->setRawRequest($r);

        return $this;
    }

    public function getRequestParam()
    {
        try {
            $r = $this->_rawRequest;
            $priceArr = [];
            $weight = 0;
            $request = $this->_request;

            if ($r->getDestPostal() && $r->getDestCity()){

              if ($request->getDestCountryId()=='ZA') {
                  $maxWeight = 30; //in Kg
              } else {
                  $maxWeight = 25;
              }

              foreach ($request->getAllItems() as $item) {
                  if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                          continue;
                  } else {
                      $weight = $weight + $item->getWeight()*$item->getQty();
                  }
              }

              $weight = $this->_convertWeight($weight); //in Kg
              $times = (int)($weight/$maxWeight);
              $additionalWeight = $weight%$maxWeight;

              if ($weight<=$maxWeight) {
                  $response = $this->getPriceLookUp($weight);
              } else {
                  $response = $this->getPriceLookUp($maxWeight, $times);
                  if (($response['type'] === 'success') && ($additionalWeight>0)) {
                      $response = $this->getPriceLookUp($additionalWeight, $times, $response['price']);
                  }
              }
              if ($response['type'] === 'success') {
                  $priceArr = $response['price'];
              }
          }
        } catch (\Exception $e) {
            $response['type']='error';
            $response['error']=$e->getMessage();
        }
        //$this->_debug($response);
        return $priceArr;
    }

    public function getPriceLookUp($weight, $times = 1, $priceArr = [])
    {

        $sellerRFCode = $this->getSellerRfCode();
        $r = $this->_rawRequest;
        $request = $this->_request;
        $city = str_replace(' ', '%20', $r->getDestCity());
        $country_code = ['1'=>'AU','6'=>'NZ','11'=>'IE','24'=>'ZA'];

        $url  = 'https://api.fastway.org/v3/psc/lookup/'.$sellerRFCode.'/'.
        $city.'/'.$r->getDestPostal().
        '/'.$weight.'?api_key='.$this->getConfigData('api_key');

        $results = json_decode($this->_getQuotesFromServer($request, $url));

        if (isset($results->result)) {
            if ($country_code[$results->result->country_code] != $request->getDestCountryId()) {
                $response['type'] = 'error';
                $response['error'] = __('Country not valid');
            } else {
                foreach ($results->result->services as $data) {
                  if ($data->type == "Parcel" && $data->labelcolour !="BLACK" && $data->labelcolour !="BLUE" && $data->labelcolour !="YELLOW" ){
                    if (!isset($priceArr[$data->labelcolour])) {
                        $resultCountryCode=$results->result->country_code;
                        $priceArr[$data->labelcolour] = [
                            'label' => $data->type."(".$data->labelcolour.")",
                            'amount' => $times * ($this->_convertAmount($resultCountryCode, $data->totalprice_frequent))
                        ];
                    } else {
                        $resultCountryCode=$results->result->country_code;
                        $convertAmount=$this->_convertAmount($resultCountryCode, $data->totalprice_frequent);
                        $priceArr[$data->labelcolour] = [
                            'label' => $data->type."(".$data->labelcolour.")",
                            'amount' => $priceArr[$data->labelcolour]['amount'] + $convertAmount
                        ];
                    }
                  }
                }
                $response['type'] = 'success';
                $response['price'] = $priceArr;
            }
        } else {
            $response['type'] = 'error';
            $response['error'] = $results->error;
        }

        return $response;
    }
    /**
     * Get shipping quotes from DHL service
     *
     * @param string $request
     * @return string
     */
    protected function _getQuotesFromServer($request, $url)
    {
        $client = $this->_httpClientFactory->create();
        try {
            $client->setUri($url);
        } catch (\Exception $e) {
            $response['type'] = 'error';
            $this->_parseXmlResponse($response);
        }
        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);

        return $client->request(\Zend_Http_Client::POST)->getBody();
    }

    protected function getSellerRfCode()
    {
        $defaultCountry = $this->getConfigData('default_country');
        $countryCode = strtolower($defaultCountry);
        $rfCode = $this->getConfigData('fr_'.$countryCode);

        return $rfCode;
    }

    /**
     * Get allowed shipping methods.
     *
     * @return string[]
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAllowedMethods()
    {
        return ['webkulfastway' => $this->getConfigData('name')];
    }
    /**
     * Do request to shipment.
     *
     * @param \Magento\Shipping\Model\Shipment\Request $request
     *
     * @return array|\Magento\Framework\DataObject
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function requestToShipment($request)
    {
        throw new \Magento\Framework\Exception\LocalizedException(
            __('Fastway does not allow to create shipping label.')
        );
    }

    /**
     * Do shipment request to carrier web service,.
     *
     * @param \Magento\Framework\DataObject $request
     *
     * @return \Magento\Framework\DataObject
     */
    public function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
        return false;
    }

    protected function _setOriginAddress()
    {
        $request = $this->_rawRequest;
        $request->setOriginPostcode(
            $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_ZIP,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            )
        );
        $request->setOriginCountryId(
            $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_COUNTRY_ID,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            )
        );
        $request->setOrigState(
            $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_REGION_ID,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            )
        );
        $request->setOriginCity(
            $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_CITY,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            )
        );
    }

    protected function _getSellerOrigin()
    {
        $request = $this->_rawRequest;
        $originPostcode = $this->_scopeConfig->getValue(
            \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_ZIP,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()->getStoreId()
        );
        $originCountryId = $this->_scopeConfig->getValue(
            \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_COUNTRY_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()->getStoreId()
        );

        return [$originPostcode, $originCountryId];
    }

    /***
     * @param $weight
     * @param $unit
     * @return string
     */
    protected function _convertWeight($weight)
    {
        if ($this->getConfigData('unit_of_measure') =='L') {
            $weight = $weight * 0.4535;
        }
        if ($weight <= 0) {
            $weight =1;
        }
        return number_format($weight, 3);
    }

    /***
     * @param $weight
     * @param $unit
     * @return string
     */
    protected function _convertAmount($from, $amount)
    {
        $country_code = ['1'=>'AUD','6'=>'NZD','11'=>'EUR','24'=>'ZAR'];
        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();

        $rates = $this->_currency->getCurrencyRates($baseCurrencyCode, [$country_code[$from]]);
        if (array_key_exists($country_code[$from], $rates)) {
            $amount = $amount/$rates[$country_code[$from]];
        }

        return number_format($amount, 3);
    }

    public function processAdditionalValidation(\Magento\Framework\DataObject $request)
    {
        return true;
    }
}

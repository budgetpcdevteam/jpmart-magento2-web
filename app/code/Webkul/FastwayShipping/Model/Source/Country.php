<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_FastwayShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\FastwayShipping\Model\Source;

use Magento\Shipping\Model\Carrier\Source\GenericInterface;

/**
 * Generic source
 */
class Country implements GenericInterface
{
    /**
     * Carrier code
     *
     * @var string
     */
    protected $code = '';
    /**
     * Returns array to be used in multiselect on back-end
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['value'=>'AU', 'label'=>'Australia'],
            ['value'=>'NZ', 'label'=>'New Zealand'],
            ['value'=>'IE', 'label'=>'Ireland'],
            ['value'=>'ZA', 'label'=>'South Africa']
        ];
        
        return $options;
    }

    public function getContryCode($code = '')
    {
        $options = [
            'AU' => 1,
            'NZ' => 6,
            'IE' => 11,
            'ZA' => 24
        ];

        return $options[$code];
    }

    public function getContryLabel($code = '')
    {
        $options = [
            'AU' => 'Australia',
            'NZ' => 'New Zealand',
            'IE' => 'Ireland',
            'ZA' => 'South Africa'
        ];

        return $options[$code];
    }
}

<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_FastwayShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\FastwayShipping\Model\Source;

use Magento\Shipping\Model\Carrier\Source\GenericInterface;

/**
 * Generic source
 */
class CountryFrInterface implements GenericInterface
{
    /**
     * Carrier code
     *
     * @var string
     */
    protected $code = '';

    /**
     * Carrier code
     *
     * @var string
     */
    protected $countryCode = '';

    /**
     * @var \Webkul\FastwayShipping\Helper\Data
     */
    protected $_helper;

    public function __construct(
        \Webkul\FastwayShipping\Helper\Data $helper
    ) {
        $this->_helper = $helper;
    }
    /**
     * Returns array to be used in multiselect on back-end
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $nzFr = $this->_helper->getFrenchies($this->countryCode);
        foreach ($nzFr as $key => $value) {
            $fr = ['value' => $key, 'label' => $value];
            array_push($options, $fr);
        }

        return $options;
    }
}

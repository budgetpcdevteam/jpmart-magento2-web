<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_FastwayShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\FastwayShipping\Model\Source;

/**
 * Generic source
 */
class SouthAfricafr extends CountryFrInterface
{
    /**
     * Carrier code
     *
     * @var string
     */
    protected $countryCode = 24;
}

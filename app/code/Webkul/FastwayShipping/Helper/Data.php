<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_FastwayShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\FastwayShipping\Helper;

/**
 * FastwayShipping data helper.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var string
     */
    protected $_code = 'webkulfastway';
    /**
     * Core store config.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @param Magento\Framework\App\Helper\Context        $context
     * @param Magento\Catalog\Model\ResourceModel\Product $product
     * @param Magento\Store\Model\StoreManagerInterface   $_storeManager
     * @param Magento\Directory\Model\Currency            $currency
     * @param Magento\Framework\Locale\CurrencyInterface  $localeCurrency
     * @param \Magento\Customer\Model\Session             $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\Order $order,
        \Webkul\FastwayShipping\Model\Source\Country $country
    ) {
        $this->country = $country;
        $this->order = $order;
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
    }
    
    /**
     * Retrieve information from carrier configuration.
     *
     * @param string $field
     *
     * @return void|false|string
     */
    public function getConfigData($field)
    {
        if (empty($this->_code)) {
            return false;
        }
        $path = 'carriers/'.$this->_code.'/'.$field;

        return $this->_scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()
        );
    }
    /**
     * check if label content exists then show print button
     * @param int  $orderId
     * @return boolean
     */
    public function _isFastwayShipment($orderId)
    {
        $customerId = $this->_customerSession->getCustomerId();
        $order = $this->order->load($orderId);
        $shippingmethod = $order->getShippingMethod();

        if (strpos($shippingmethod, 'mparamex') !== false) {
            $sellerOrders = $this->_objectManager->create(
                \Webkul\Marketplace\Model\Orders::class
            )->getCollection()
            ->addFieldToFilter('seller_id', ['eq' => $customerId])
            ->addFieldToFilter('order_id', ['eq' => $orderId]);

            $labelContent = '';
            foreach ($sellerOrders as $row) {
                $labelContent = $row->getShipmentLabel();
            }

            if ($labelContent != '') {
                return true;
            }
        } elseif (strpos($shippingmethod, 'mp_multishipping') !== false) {
            $sellerOrders = $this->_objectManager->create(
                \Webkul\Marketplace\Model\Orders::class
            )->getCollection()
            ->addFieldToFilter('seller_id', ['eq' => $customerId])
            ->addFieldToFilter('order_id', ['eq' => $orderId]);

            $labelContent = '';
            $method = '';
            foreach ($sellerOrders as $row) {
                $labelContent = $row->getShipmentLabel();
                $method = $row->getMultishipMethod();
            }

            if ($labelContent != '' && strpos($method, 'mparamex') !== false) {
                return true;
            }
        }
        return false;
    }

   /**
    * return current customer session.
    *
    * @return \Magento\Customer\Model\Session
    */
    public function _getCustomerData()
    {
        return $this->_customerSession->getCustomer();
    }

    /**
     * @return array
     */
    public function getCountry()
    {
        return $this->country->toOptionArray();
    }

    /**
     * @return array
     */
    public function getCountryCode($code)
    {
        return $this->country->getContryCode($code);
    }

    /**
     * @return array
     */
    public function getContryLabel($code)
    {
        return $this->country->getContryLabel($code);
    }

    public function getFrenchies($country = '')
    {
        $frList = [
            '1' => [
                'ADL' => 'Adelaide',
                'ALB' => 'Albury',
                'BEN' => 'Bendigo',
                'BRI' => 'Brisbane',
                'CNS' => 'Cairns',
                'CBR' => 'Canberra',
                'CAP' => 'Capricorn Coast',
                'CCT' => 'Central Coast',
                'CFS' => 'Coffs Harbour',
                'GEE' => 'Geelong',
                'GLD' => 'Gold Coast',
                'TAS' => 'Hobart',
                'LAU' => 'Launceston',
                'LEA' => 'Linehaul Express Australia',
                'MKY' => 'Mackay',
                'MEL' => 'Melbourne',
                'NEW' => 'Newcastle',
                'NTH' => 'Northern Rivers',
                'OAG' => 'Orange',
                'PER' => 'Perth',
                'PQQ' => 'Port Macquarie',
                'SUN' => 'Sunshine Coast',
                'SYD' => 'Sydney',
                'TMW' => 'Tamworth',
                'TOO' => 'Toowoomba',
                'TVL' => 'Townsville',
                'BDB' => 'Wide Bay',
                'WOL' => 'Wollongong',
            ],
            '6' => [
                'ASH' => 'Ashburton',
                'AUK' => 'Auckland',
                'BOI' => 'Bay of Islands',
                'BLH' => 'Blenheim',
                'CSI' => 'Central South Island',
                'CHC' => 'Christchurch',
                'CES' => 'Customer Experience Department',
                'DUN' => 'Dunedin',
                'EBP' => 'EBOP',
                'FNT' => 'Far North',
                'GIS' => 'Gisborne',
                'GRE' => 'Gore',
                'HLZ' => 'Hamilton',
                'HWA' => 'Hawera',
                'NPE' => 'Hawkes Bay',
                'IVC' => 'Invercargill',
                'KAP' => 'Kapiti',
                'MAS' => 'Masterton',
                'NSN' => 'Nelson',
                'NPL' => 'New Plymouth',
                'NSA' => 'North Shore',
                'OAM' => 'Oamaru',
                'PMR' => 'Palmerston North',
                'ZQN' => 'Queenstown',
                'ROT' => 'Rotorua',
                'TUO' => 'Taupo',
                'TGA' => 'Tauranga',
                'TIM' => 'Timaru',
                'TOK' => 'Tokoroa',
                'WIR' => 'Wairoa',
                'WAG' => 'Wanganui',
                'WRK' => 'Warkworth',
                'WEL' => 'Wellington',
                'WES' => 'West Coast',
                'WSZ' => 'Westport',
                'WRE' => 'Whangarei',
            ],
            '11' => [
                'CRK' => 'Cork',
                'DON' => 'Donegal',
                'DUB' => 'Dublin',
                'GWY' => 'Galway',
                'GB1' => 'GB Zone 1',
                'GB2' => 'GB Zone 2',
                'KER' => 'Kerry',
                'LMK' => 'Limerick',
                'MAY' => 'Mayo',
                'MID' => 'Midlands',
                'NEA' => 'North East',
                'NWE' => 'North West',
                'NIE' => 'Northern Ireland East',
                'NIW' => 'Northern Ireland West',
                'SLG' => 'Sligo',
                'SEA' => 'South East',
                'SWE' => 'South West',
                'TIP' => 'Tipperary',
                'WIE' => 'West',
                'WMH' => 'Westmeath',
                'WEX' => 'Wexford',
                'WIC' => 'Wicklow',
            ],
            '24' => [
                'BFN' => 'Bloemfontein',
                'CPT' => 'Cape Town',
                'DUR' => 'Durban',
                'ELS' => 'East London',
                'GRJ' => 'George',
                'HRS' => 'Harrismith',
                'JNB' => 'Johannesburg',
                'KDP' => 'Klerksdorp',
                'NLP' => 'Nelspruit',
                'PTG' => 'Polokwane',
                'PLZ' => 'Port Elizabeth',
                'PRY' => 'Pretoria',
                'RBG' => 'Rustenburg',
                'VEE' => 'Vereeniging',
                'WLK' => 'Welkom',
                'WIT' => 'Witbank',
            ]
        ];
        if ($country != '') {
            return $frList[$country];
        } else {
            return $frList;
        }
    }
}

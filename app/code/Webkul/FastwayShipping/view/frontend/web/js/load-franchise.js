/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_FastwayShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
        "jquery",
        'ko',
        'mage/translate',
        "mage/template",
        "mage/mage",
        "prototype",
    ], function ($, ko, $t,mageTemplate, alert) {
        'use strict';
        var frList = ko.observableArray([]);
        $.widget('mage.loadFranchise', {

            _create: function () {
                var self = this;
                $(self.options.country).on('change', function () {
                    if ($(this).val() != '') {
                        self.collectData($(this).val());
                    }
                });

                $('document').ready(function () {
                    if (self.options.code != '') {
                        self.collectData(self.options.code);
                    }
                });
            },

            collectData: function (params) {
                var self = this;
                var countryCode;
                countryCode = params;
                var progressTmpl = mageTemplate(self.options.optionTemplate),
                                      tmpl;
                $('#franchise-list').html('');
                $.each(self.options.frenchies, function (i, v) {
                    if (i == countryCode) {
                        $.each(v, function (code, name) {
                            var select = '';
                            if (self.options.frCode == code) {
                                select = 'selected="selected"';
                            };
                            tmpl = progressTmpl({
                                data: {
                                    code: code,
                                    name: name,
                                    select:select,
                                }
                            });
                            $('#franchise-list').append(tmpl);
                        });
                    }
                });
            }
        });
        return $.mage.loadFranchise;
    });
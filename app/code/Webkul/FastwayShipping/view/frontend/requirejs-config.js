/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_FastwayShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 /*jshint jquery:true*/
var config = {
    map: {
        '*': {
            'Magento_Checkout/js/model/cart/totals-processor/default': 'Webkul_FastwayShipping/js/model/cart/totals-processor/default'
        }
    }
};

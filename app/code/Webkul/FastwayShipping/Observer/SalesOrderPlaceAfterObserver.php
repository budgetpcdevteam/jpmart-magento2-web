<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_FastwayShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\FastwayShipping\Observer;

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager;

/**
 * Webkul FastwayShipping SalesOrderPlaceAfterObserver Observer Model
 *
 * @author      Webkul Software
 *
 */
class SalesOrderPlaceAfterObserver implements ObserverInterface
{
    /**
     * @var eventManager
     */
    protected $_eventManager;
    /**
     * @var Session
     */
    protected $_customerSession;
    /**
     * @var SessionManager
     */
    protected $_session;
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @param \Magento\Framework\Event\Manager $eventManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Logger\Monolog $logger
     * @param SessionManager $session
     */
    public function __construct(
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Logger\Monolog $logger,
        SessionManager $session
    ) {
        $this->_eventManager = $eventManager;
        $this->_customerSession = $customerSession;
        $this->_logger = $logger;
        $this->_session = $session;
    }

    /**
     * after place order event handler
     * Distribute Shipping Price for sellers
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getOrder();
        $shippingmethod=$order->getShippingMethod();
        $lastOrderId = $observer->getOrder()->getId();
        if (strpos($shippingmethod, 'webkulfastway')!==false) {
            $this->_session->unsShippingInfo();
        }
    }
}

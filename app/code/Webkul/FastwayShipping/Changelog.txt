
-------------------------------- Version 3.0.0 --------------------------------------

	+ Version updated for Magento 2.3

-------------------------------- Version 2.0.1 --------------------------------------
	+ Price Calculation for weight above 25Kg implemented.
	- Bugs fixed.

-------------------------------- Version 2.0.0 --------------------------------------

	+ Compatible with Magento v2.2.*
	+ Calculate Shipping Cost According to Fastway Shipping.
	+ Admin can enable and disable the Module.
	+ Shipping Rate depends on Admin's Regional Franchise and Product weight.
